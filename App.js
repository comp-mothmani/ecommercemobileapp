import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import loadingScreen from './src/loadingScreen';
import loginScreen from './src/loginScreen';
import homeScreen2 from './src/homeScreen2';
import categoryScreen from './src/categoryScreen';
import registerScreen from './src/registerScreen';
import basketScreen from './src/basketScreen';
import detailScreen from './src/detailScreen';
import purchaseScreen from './src/PurchaseScreen';
import mapsScreen from './src/mapsScreen';
import UpdateScreen from './src/UpdateScreen';

const AppStack = createStackNavigator({
  Home: {
    screen: homeScreen2,
    navigationOptions: {
      headerShown: false,
    },
  },
  Category: {
    screen: categoryScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Purchase: {
    screen: purchaseScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Basket: {
    screen: basketScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Maps: {
    screen: mapsScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Detail: {
    screen: detailScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Register: {
    screen: registerScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  Login: {
    screen: loginScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
  UpdateScreen: {
    screen: UpdateScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});

const AuthStack = createStackNavigator({
  Login: loginScreen,
  Register: registerScreen,
});

/** GLOBAL VARIABLES, CAN BE ACCESSED FROM ANY CLASS **/
global.listOfProducts = [];
global.defaultListOfProducts = [];
global.totalPrice = '0 TND';
global.ngrok = 'http://4014f825152f.ngrok.io';
global.userCriteria = [];
/**               GLOBAL VARIABLES                   **/

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: loadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'Loading',
    },
  ),
);
