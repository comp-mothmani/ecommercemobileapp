/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  ScrollView,
  Button,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {createAppContainer} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';

import {
  IndicatorViewPager,
  PagerDotIndicator,
} from 'react-native-best-viewpager';
import oldPurchasesScreen from './oldPurchasesScreen';

const numColumns = 4;

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
    numberOfElementsLastRow++;
  }

  return data;
};

class homeScreen extends React.Component {
  constructor(props) {
    super(props);

    //setting default state
    this.state = {isLoading: true, text: ''};
    this.arrayholder = [];
  }

  componentDidMount() {
    return fetch(global.ngrok + '/categories')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function() {
            this.arrayholder = responseJson;
          },
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  static navigationOptions = {
    headerShown: false,
  };

  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter(function(item) {
      //applying filter for the inserted text in search bar
      const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      text: text,
    });
  }

  renderItem = ({item}) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }

    return (
      <View style={styles.item}>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            margin: 8,
            paddingTop: 8,
            paddingBottom: 8, //flex: 1,
            backgroundColor: '#d7deeb',
            borderRadius: 10,
          }}
          activeOpacity={0.5}
          onPress={() =>
            this.props.navigation.navigate('Category', {
              categoryName: item.name,
              categoryId: item.id,
            })
          }>
          <Image
            source={{uri: 'data:image/png;base64,' + item.picture}}
            style={styles.stretch}
          />
        </TouchableOpacity>

        <Text style={{color: '#005288', textAlign: 'center', fontSize: 15}}>
          {item.name}
        </Text>
        <Text
          style={styles.itemText}
          onPress={() => this.props.navigation.navigate('Detail')}>
          {item.Prix}
        </Text>
      </View>
    );
  };

  render() {
    if (this.state.isLoading) {
      //Loading View while data is loading
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{
          width: '100%',
          height: '100%',
        }}>
        <View style={styles.container}>
          <View style={styles.navBar}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Icon name={'search'} color={'#1e90ff'} size={30} />
            </View>

            <TextInput
              style={styles.searchInput}
              placeholder={'Search Categories'}
              placeholderTextColor={'#808080'}
              underlineColorAndroid="transparent"
              onChangeText={text => this.SearchFilterFunction(text)}
            />

            <TouchableOpacity
              style={styles.pannierIcon}
              onPress={() => this.props.navigation.navigate('Basket')}>
              <Icon name={'local-grocery-store'} color={'#1e90ff'} size={30} />
            </TouchableOpacity>
          </View>

          <IndicatorViewPager
            style={{
              margin: '2%',
              marginLeft: '5%',
              marginRight: '5%',
              height: '20%',
              width: '97%',
              alignSelf: 'center',
            }}
            //style={{flex: 1, backgroundColor: 'red', margin: 10}}

            indicator={this._renderDotIndicator()}>
            <View
              style={{
                borderRadius: 10,

                backgroundColor: '#C70039',
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 10,
                }}
                source={require('../picture/pub1.png')}
              />
            </View>
            <View
              style={{
                borderRadius: 10,
                backgroundColor: '#C70039',
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 10,
                }}
                source={require('../picture/pub2.jpg')}
              />
            </View>
            <View
              style={{
                borderRadius: 10,
                backgroundColor: '#C70039',
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 10,
                }}
                source={require('../picture/pub3.jpg')}
              />
            </View>
          </IndicatorViewPager>

          <FlatList
            style={{
              height: '65%',
              width: '100%',
              margin: '2%',
              marginLeft: '5%',
              marginRight: '5%',
              alignSelf: 'center',
            }}
            contentContainerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
            data={formatData(this.state.dataSource, numColumns)}
            renderItem={this.renderItem}
            numColumns={numColumns}
            keyExtractor={(item, index) => index}
          />
        </View>
      </ImageBackground>
    );
  }
  _renderDotIndicator() {
    return <PagerDotIndicator pageCount={3} />;
  }
}

class profileScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentPassword: '',
      newPassword: '',
      newEmail: '',
      newName: '',
      newFirstName: '',
      newNumber: '',
      newAddress: '',
      username: null,
      bol: '',
      name: '',
      firstname: '',
      adresse: '',
      phonenumber: '',
      id: '',
    };
  }
  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <View>
          <Text style={styles.textLogout}> Profile Screen</Text>
        </View>
      </ImageBackground>
    );
  }
}

/*class historyScreen extends React.Component {
  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <View>
          <Text style={styles.textLogout}> historyScreen</Text>
        </View>
      </ImageBackground>
    );
  }
}*/

class infoScreen extends React.Component {
  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <View>
          <Text style={styles.textLogout}> infoScreen</Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  navBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFF',
    margin: 5,
    width: '97%',
    height: '10%',
    borderRadius: 10,
  },
  searchIcon: {},
  searchInput: {
    fontSize: 17,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pannierIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logOutText: {
    color: '#FFF',
    fontSize: 50,
  },
  textLogout: {
    color: '#FFF',
    fontSize: 20,
    textAlign: 'center',
  },
  viewPager: {
    backgroundColor: '#FFF',
    margin: 10,
    marginLeft: 6,
    width: 350,
    height: 250,
    borderRadius: 10,
  },
  stretch: {
    width: 40,
    height: 50,
    borderRadius: 10,
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemText1: {
    marginLeft: 10,
    fontWeight: 'bold',
  },

  itemText2: {
    fontWeight: 'bold',
    marginLeft: 20,
  },

  itemText3: {
    fontWeight: 'bold',
    marginLeft: 22,
  },

  itemText4: {
    fontWeight: 'bold',
    marginLeft: 20,
  },

  itemText5: {
    fontWeight: 'bold',
    marginLeft: 25,
  },

  itemText6: {
    fontWeight: 'bold',
    marginLeft: 7,
  },
  view1: {
    marginLeft: 10,
    marginTop: -5,
  },
  view2: {
    marginLeft: 120,
    marginTop: -238,
  },

  view3: {
    marginLeft: 230,
    marginTop: -238,
  },
  title: {
    color: '#1e90ff',
    fontWeight: 'bold',
    fontStyle: 'italic',
    marginLeft: 10,
    fontSize: 30,
  },
  button: {
    marginTop: 20,
    backgroundColor: '#1e90ff',
    borderRadius: 4,
    height: '13%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '90%',
    marginLeft: 20,
    marginRight: 20,
  },

  textInput: {
    borderWidth: 1,
    borderColor: 'white',
    backgroundColor: '#FFF',
    marginVertical: 20,
    padding: 10,
    height: 40,
    alignSelf: 'stretch',
    fontSize: 18,
  },
  text: {
    fontSize: 20,
    color: '#000000',
    top: 70,
  },
  textName: {
    fontSize: 30,
    color: '#000000',
    top: 50,
  },

  buttonEDit: {
    marginHorizontal: 10,
    backgroundColor: '#1e90ff',
    borderRadius: 25,
    height: '10%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '40%',
    marginLeft: 195,
    top: -170,
  },
  buttonMYP: {
    marginHorizontal: 10,
    backgroundColor: '#1e90ff',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    width: '40%',
    height: '10%',

    top: 110,
  },
  buttonLoc: {
    marginHorizontal: 10,
    backgroundColor: '#1e90ff',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    width: '40%',
    height: '10%',
    top: 100,
  },
  buttonSOUT: {
    marginHorizontal: 10,
    backgroundColor: '#1e90ff',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    width: '40%',
    height: '10%',
    top: 120,
  },
  ViewProfile: {
    backgroundColor: '#f0edf9',
    marginTop: 180,
    height: '75%',
  },
});
const TabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: homeScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'home'} />
          </View>
        ),
      },
    },
    Profile: {
      screen: profileScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'person'} />
          </View>
        ),
      },
    },
    History: {
      screen: oldPurchasesScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon style={[{color: tintColor}]} size={25} name={'history'} />
          </View>
        ),
      },
    },
    Info: {
      screen: infoScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View>
            <Icon
              style={[{color: tintColor}]}
              size={25}
              name={'info-outline'}
            />
          </View>
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    activeColor: '#0000ff',
    inactiveColor: '#1e90ff',
    barStyle: {backgroundColor: '#f0edf9'},
  },
);
export default createAppContainer(TabNavigator);
