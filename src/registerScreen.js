import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  StatusBar,
  KeyboardAvoidingView,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class registerScreen extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  state = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    number: '',
    Address: '',
    errorMessage: null,
    showPass: true,
    press: false,
    UserID: '',
  };

  showPass = () => {
    if (this.state.press == false) {
      this.setState({showPass: false, press: true});
    } else {
      this.setState({showPass: true, press: false});
    }
  };

  handleSignUp = () => {
    fetch(
      global.ngrok +
        '/users/create' +
        '/' +
        this.state.firstName +
        '/' +
        this.state.lastName +
        '/' +
        this.state.email +
        '/' +
        this.state.password +
        '/' +
        this.state.number +
        '/' +
        this.state.Address,
      {
        method: 'GET', //Request Type
      },
    )
      .then(response => response.json())
      //If response is in json then in success
      .then(responseJson => {
        //Success
        console.log(responseJson);

        if (JSON.stringify(responseJson) === '200') {
          ToastAndroid.show(
            'USER: ' +
              this.state.firstName +
              ' ' +
              this.state.lastName +
              ' Successfully created',
            ToastAndroid.SHORT,
          );
          this.props.navigation.navigate('Login');
        } else if (JSON.stringify(responseJson) === '100') {
          ToastAndroid.show('THIS ADDRESS IS ALREADY USED', ToastAndroid.SHORT);
        } else {
          ToastAndroid.show(
            'USER: ' +
              this.state.firstName +
              ' ' +
              this.state.lastName +
              ' failed to be created',
            ToastAndroid.SHORT,
          );
        }
      })
      //If response is not in json then in error
      .catch(error => {
        //Error
        ToastAndroid.show(
          'USER: ' +
            this.state.firstName +
            ' ' +
            this.state.lastName +
            ' failed to be created',
          ToastAndroid.SHORT,
        );
        console.log(error);
      });
  };

  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Login')}>
          <Icon name={'navigate-before'} size={40} style={styles.inputIcons} />
        </TouchableOpacity>

        <KeyboardAvoidingView
          style={styles.containerKeyBoard}
          behavior="padding">
          <StatusBar barStyle="light-content" />

          <View style={styles.errorMessage}>
            {this.state.errorMessage && (
              <Text style={styles.error}>{this.state.errorMessage}</Text>
            )}
          </View>
          <View style={styles.form}>
            <View style={{marginTop: -30}}>
              <Text style={styles.inputTitle}> First name</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={firstname =>
                  this.setState({firstName: firstname})
                }
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text style={styles.inputTitle}> Last name</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={lastName => this.setState({lastName: lastName})}
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text style={styles.inputTitle}> Email Address</Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={email => this.setState({email: email})}
              />
            </View>

            <View style={{marginTop: 10}}>
              <Text style={styles.inputTitle}> Password </Text>
              <TextInput
                style={styles.input}
                secureTextEntry={this.state.showPass}
                autoCapitalize="none"
                onChangeText={password => this.setState({password: password})}
              />
              <TouchableOpacity
                style={styles.btnEye}
                onPress={this.showPass.bind(this)}>
                <Icon
                  name={
                    this.state.press == false ? 'visibility' : 'visibility-off'
                  }
                  color={'#000000'}
                  size={25}
                />
              </TouchableOpacity>
            </View>
            <View style={{marginTop: 10}}>
              <Text style={styles.inputTitle}>Phone Number </Text>
              <TextInput
                style={styles.input}
                keyboardType="number-pad"
                autoCapitalize="none"
                onChangeText={number => this.setState({number: number})}
              />
            </View>

            <View style={{marginTop: 10}}>
              <Text style={styles.inputTitle}> Address </Text>
              <TextInput
                style={styles.input}
                autoCapitalize="none"
                onChangeText={Address => this.setState({Address: Address})}
              />
            </View>
          </View>

          <TouchableOpacity style={styles.button} onPress={this.handleSignUp}>
            <Text style={{color: '#FFF', fontWeight: '500'}}> Sign up </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{alignSelf: 'center', marginTop: 32}}
            onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={{color: '#000000', fontSize: 16}}>
              New to MyApp ?{' '}
              <Text style={{color: '#000000', fontWeight: '500'}}> Login</Text>
            </Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerKeyBoard: {
    flex: 1,
    justifyContent: 'space-between',
  },
  inputIcons: {
    marginTop: 30,
    marginLeft: 10,
    color: '#000000',
  },

  greeting: {
    marginTop: 32,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    color: '#000000',
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  error: {
    color: '#000000',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
  },
  inputTitle: {
    color: '#000000',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  input: {
    borderBottomColor: '#000000',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 40,
    fontSize: 15,
    color: '#000000',
  },
  btnEye: {
    position: 'absolute',
    top: 20,
    right: 15,
  },
  button: {
    marginHorizontal: 10,
    backgroundColor: '#1e90ff',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
