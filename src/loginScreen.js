import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  StatusBar,
  Image,
  ImageBackground,
  TouchableOpacity,
  Button,
  ToastAndroid,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class loginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: 'email',
      userPassword: 'pass',
      showPass: true,
      press: false,
    };
  }

  static navigationOptions = {
    headerShown: false,
  };

  handleLogin = (userEmail, userPassword) => {
    fetch(global.ngrok + '/users/check/' + userEmail + '/' + userPassword, {
      method: 'GET',
      //Request Type
    })
      .then(response => response.json())
      //If response is in json then in success
      .then(responseJson => {
        //Success
        console.log(responseJson);
        global.userCriteria = responseJson;
        this.props.navigation.navigate('Home');
      })
      //If response is not in json then in error
      .catch(error => {
        //Error
        ToastAndroid.show('WRONG CREDENTIALS', ToastAndroid.SHORT);
        console.log(error);
      });
  };

  showPass = () => {
    if (this.state.press == false) {
      this.setState({showPass: false, press: true});
    } else {
      this.setState({showPass: true, press: false});
    }
  };
  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <Image
            source={require('../picture/30.png')}
            style={{
              alignSelf: 'center',
              borderRadius: 30,
              marginTop: 90,
              width: 90,
              height: 90,
            }}
          />

          <View style={{marginTop: 10}}>
            <Text style={styles.inputTitle}> Email Address</Text>
            <TextInput
              style={styles.input}
              placeholder="Email"
              autoCapitalize="none"
              onChangeText={email => this.setState({userEmail: email})}
            />
          </View>

          <View style={{marginTop: 10}}>
            <Text style={styles.inputTitle}> Password </Text>
            <TextInput
              style={styles.input}
              secureTextEntry={this.state.showPass}
              autoCapitalize="none"
              onChangeText={password => this.setState({userPassword: password})}
            />
            <TouchableOpacity
              style={styles.btnEye}
              onPress={this.showPass.bind(this)}>
              <Icon
                name={
                  this.state.press == false ? 'visibility' : 'visibility-off'
                }
                color={'#000000'}
                size={25}
              />
            </TouchableOpacity>
          </View>

          <Button
            style={{fontSize: 20, color: 'green'}}
            styleDisabled={{color: 'red'}}
            onPress={() =>
              this.handleLogin(this.state.userEmail, this.state.userPassword)
            }
            title="Login"
          />

          <TouchableOpacity
            style={{alignSelf: 'center', marginTop: 32}}
            onPress={() => this.props.navigation.navigate('Register')}>
            <Text style={{color: '#000000', fontSize: 16}}>
              New to MyApp ?
              <Text style={{color: '#000000', fontWeight: '500'}}>
                {' '}
                Sign Up
              </Text>
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: -10,
  },
  greeting: {
    marginTop: 32,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    color: '#000000',
  },
  errorMessage: {
    height: 72,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  error: {
    color: '#000000',
    fontSize: 13,
    fontWeight: '600',
    textAlign: 'center',
  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
  },
  inputTitle: {
    color: '#000000',
    fontSize: 10,
    textTransform: 'uppercase',
  },
  input: {
    borderBottomColor: '#000000',
    borderBottomWidth: StyleSheet.hairlineWidth,
    height: 40,
    fontSize: 15,
    color: '#000000',
  },
  btnEye: {
    position: 'absolute',
    top: 20,
    right: 15,
  },
  button: {
    marginHorizontal: 30,
    backgroundColor: '#1e90ff',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
