import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  ToastAndroid,
  Image,
} from 'react-native';
import NumericInput from 'react-native-numeric-input';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  JsonClassType,
  JsonProperty,
  ObjectMapper,
  JsonIdentityInfo,
  ObjectIdGenerator,
} from 'jackson-js';

export let instance = null;

export default class basketScreen extends Component {
  constructor(props) {
    super(props);

    instance = this;

    this.state = {
      k: 0,
      inc: 1,
      total: 0,
    };

    this.updateTotalPrice();

    /*for (var i = 0; i < global.listOfProducts.length; i++) {
      var name = global.listOfProducts[i].productName;
    }*/

    /*var myJSON = JSON.stringify(global.listOfProducts);
    console.log('ALL PRODUCTS: ' + myJSON);
    var obj = JSON.parse(myJSON);*/
  }

  updateTotalPrice() {
    var i;
    var arrayLength = global.listOfProducts.length;
    var sum = 0;

    for (i = 0; i < arrayLength; i++) {
      // eslint-disable-next-line radix
      sum += parseInt(
        global.listOfProducts[i].productPrice.substring(
          0,
          global.listOfProducts[i].productPrice.length - 4,
        ),
      );
    }
    if (sum === 0) {
      global.totalPrice = '0 TND';
    } else {
      global.totalPrice = sum + ' TND';
    }
  }

  deleteItem(text) {
    const result = global.listOfProducts.find(
      item => item.productName === text,
    );

    const index = global.listOfProducts.indexOf(result);

    if (index > -1) {
      global.listOfProducts.splice(index, 1);
    }

    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: global.listOfProducts,
    });

    this.updateTotalPrice();
  }

  updatePrice(value, productName) {
    //this.setState({price:(value*px)+ " TND"})
    console.log('value) ' + value);
    const defaultObject = global.defaultListOfProducts.find(
      item => item.defaultProductName === productName,
    );
    const result = global.listOfProducts.find(
      item => item.productName === productName,
    );

    const px = defaultObject.defaultProductPrice.substring(
      0,
      defaultObject.defaultProductPrice.length - 4,
    );

    if (value === 1) {
      result.productQuantity = value;
      result.productPrice = defaultObject.defaultProductPrice;
      this.setState({
        //setting the filtered newData on datasource
        //After setting the data it will automatically re-render the view
        dataSource: global.listOfProducts,
      });
      /* this.updatePrice(); // update price of all products
      global.totalPrice =
        global.totalPrice.substring(0, global.totalPrice.length - 4) -
        beforeAddRemovePrice.substring(0, beforeAddRemovePrice.length - 4) +
        result.productPrice.substring(0, result.productPrice.length - 4);*/

      return result.productPrice;
    } else {
      result.productQuantity = value;
      result.productPrice = px * value + ' TND';
      this.setState({
        //setting the filtered newData on datasource
        //After setting the data it will automatically re-render the view
        dataSource: global.listOfProducts,
      });
      /*this.updatePrice(); // update price of all products
      global.totalPrice =
        parseInt(global.totalPrice.substring(0, global.totalPrice.length - 4)) -
        parseInt(
          beforeAddRemovePrice.substring(0, beforeAddRemovePrice.length - 4),
        ) +
        parseInt(
          result.productPrice.substring(0, result.productPrice.length - 4),
        ) +
        ' TND';*/

      console.log('result.productPrice: ' + result.productPrice);
      return result.productPrice;
    }
  }

  confirmOrderFunc = () => {
    console.log('typeof: ' + typeof global.listOfProducts);

    var myJSON = JSON.stringify(global.listOfProducts);
    /*const objectMapper = new ObjectMapper();
    const jsonData = objectMapper.stringify < Object > global.listOfProducts;
    console.log('objct Mapper ' + jsonData);*/

    var xhttp1 = new XMLHttpRequest();
    xhttp1.withCredentials = true;
    var params = 'purchase=' + myJSON + '&userId=' + global.userCriteria.id;
    function handler1() {
      if (this.status == 200) {
        console.log('sucess');
        ToastAndroid.show('Order is saved succesfully', ToastAndroid.SHORT);
        global.listOfProducts.pop();
        instance.props.navigation.navigate('Home');
      } else {
        ToastAndroid.show('Error passing this order', ToastAndroid.SHORT);
        console.log('error');
      }
    }
    xhttp1.onload = handler1;
    xhttp1.open('POST', global.ngrok + '/puchases/save/', true);
    xhttp1.withCredentials = false;
    xhttp1.setRequestHeader(
      'content-type',
      'application/x-www-form-urlencoded;charset=UTF-8',
    );
    xhttp1.send(params);
  };

  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <TouchableOpacity
          style={styles.backIcon}
          onPress={() => this.props.navigation.navigate('Home')}>
          <Icon name={'navigate-before'} color={'#000000'} size={40} />
        </TouchableOpacity>

        <View style={styles.parentContainer}>
          <Text style={styles.text}> Cart</Text>

          <FlatList
            style={styles.flatView}
            data={global.listOfProducts}
            renderItem={({item}) => (
              <View style={styles.viewBas}>
                <Text style={styles.stretchName}>{item.productName}</Text>
                <Text style={styles.stretchPrice}>{item.productPrice}</Text>
                <Image
                  source={{uri: 'data:image/png;base64,' + item.productPic}}
                  style={styles.stretchPic}
                />
                <TouchableOpacity
                  style={styles.delateIcon}
                  onPress={() => {
                    this.deleteItem(item.productName, item.productPrice);
                  }}>
                  <Icon name={'delete'} color={'#ff0000'} size={30} />
                </TouchableOpacity>
                <View style={styles.viewQte}>
                  <NumericInput
                    NumericInput
                    onChange={value =>
                      this.updatePrice(value, item.productName)
                    }
                    totalWidth={100}
                    totalHeight={25}
                    ref="qteproduct"
                    iconSize={25}
                    maxValue={200}
                    minValue={1}
                    initValue={item.productQuantity}
                    step={1}
                    valueType="real"
                    rounded
                    textColor="#000000"
                    iconStyle={{color: 'white'}}
                    rightButtonBackgroundColor="#a9a9a9"
                    leftButtonBackgroundColor="#a9a9a9"
                  />
                </View>
              </View>
            )}
            keyExtractor={(item, index) => index}
          />
          <View style={styles.TotalView}>
            <Text style={styles.Total}> {global.totalPrice} </Text>
          </View>

          <TouchableOpacity
            style={styles.button}
            onPress={
              () => this.confirmOrderFunc()
              /*ToastAndroid.show('Payment at home delivery', ToastAndroid.SHORT)*/
            }>
            <Text style={styles.text}> Confirm order </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  parentContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewBas: {
    width: 343,
    height: 90,
    backgroundColor: '#FFF',
    margin: 10,
    borderRadius: 15,
  },
  Total: {
    fontSize: 30,
    marginLeft: 20,
    fontWeight: 'bold',
  },
  TotalView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '7%',
    backgroundColor: '#FFF',
    borderRadius: 0,
  },
  flatView: {
    height: '70%',
  },
  stretchName: {
    margin: 15,
    fontSize: 20,
    marginLeft: 100,
  },
  stretchPrice: {
    fontSize: 15,
    marginLeft: 275,
    margin: -40,
    fontWeight: 'bold',
  },
  delateIcon: {
    marginLeft: 300,
    margin: -22,
  },
  viewQte: {
    marginLeft: 100,
    margin: -5,
  },
  decreaseIcon: {
    marginLeft: 5,
    top: -1,
  },
  increaseIcon: {
    marginLeft: 45,
    top: -42,
  },
  qte: {
    top: -22,
    marginLeft: 25,
    fontWeight: 'bold',
  },

  stretchPic: {
    marginLeft: 10,
    width: 70,
    height: 70,
    marginTop: 10,
  },
  backIcon: {
    top: 30,
    left: 20,
  },
  button: {
    backgroundColor: '#1e90ff',
    borderRadius: 0,
    width: '100%',
    height: '7%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 24,
    textAlign: 'center',
    color: '#000000',
    alignItems: 'center',
  },
  addicon: {
    top: -22,
    marginLeft: 72,
  },
});
