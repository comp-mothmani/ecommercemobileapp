/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Image,
  Alert,
  TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

const numColumnss = 3;

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
    numberOfElementsLastRow++;
  }

  return data;
};

export default class categoryScreen extends React.Component {
  constructor(props) {
    super(props);

    //setting default state
    this.state = {
      isLoading: true,
      text: '',
      categoryName: this.props.navigation.state.params.categoryName,
      categoryId: this.props.navigation.state.params.categoryId,
    };
    this.arrayholder = [];
  }

  SearchFilterFunction(text) {
    //passing the inserted text in textinput
    const newData = this.arrayholder.filter(function(item) {
      //applying filter for the inserted text in search bar
      const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      dataSource: newData,
      text: text,
    });
  }

  componentDidMount() {
    return fetch(global.ngrok + '/categories/' + this.state.categoryName)
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson,
          },
          function() {
            this.arrayholder = responseJson;
          },
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  static navigationOptions = {
    headerShown: false,
  };
  state = {
    value: '',
  };

  receivedValue = value => {
    this.setState({value});
  };

  _handleIndexChange = index => this.setState({index});

  renderItem = ({item, index}) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />;
    }
    return (
      <View style={styles.item}>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            margin: 20,
            paddingTop: 8,
            paddingBottom: 8, //flex: 1,
            backgroundColor: '#d7deeb',
            borderRadius: 10,
          }}
          activeOpacity={0.5}
          onPress={() =>
            this.props.navigation.navigate('Purchase', {
              subCategoryName: item.name,
              subCategoryPic: item.pic,
              subCategoryDescription: item.description,
              subCategoryPrice: item.price,
              subCategoryId: item.id,
            })
          }>
          <Image
            source={{uri: 'data:image/png;base64,' + item.pic}}
            style={styles.stretch}
          />
        </TouchableOpacity>

        <Text
          style={styles.itemText}
          onPress={() =>
            this.props.navigation.navigate('Detail', {Nom: 'Nom'})
          }>
          {item.name}
        </Text>
        <Text
          style={styles.itemText}
          onPress={() => this.props.navigation.navigate('Detail')}>
          {item.price}
        </Text>
      </View>
    );
  };

  GetGridViewItem(fruit_name) {
    Alert.alert(fruit_name);
  }
  /*componentDidMount() {
    const {email, displayName} = firebase.auth().currentUser;

    this.setState({email, displayName});
  }*/

  render() {
    if (this.state.isLoading) {
      //Loading View while data is loading
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{
          width: '100%',
          height: '100%',
        }}>
        <View style={styles.container}>
          <View style={styles.navBar}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Icon name={'search'} color={'#1e90ff'} size={30} />
            </View>

            <TextInput
              style={styles.searchInput}
              placeholder={' Search Products '}
              placeholderTextColor={'#808080'}
              underlineColorAndroid="transparent"
              onChangeText={text => this.SearchFilterFunction(text)}
            />

            <TouchableOpacity
              style={styles.pannierIcon}
              onPress={() => this.props.navigation.navigate('Basket')}>
              <Icon name={'local-grocery-store'} color={'#1e90ff'} size={30} />
            </TouchableOpacity>
          </View>
          <FlatList
            style={{
              height: '50%',
              width: '100%',
            }}
            contentContainerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
            data={formatData(this.state.dataSource, numColumnss)}
            renderItem={this.renderItem}
            numColumns={numColumnss}
            keyExtractor={(item, index) => index}
          />
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFF',
    margin: 5,
    width: '97%',
    height: '10%',
    borderRadius: 10,
  },
  searchIcon: {},
  searchInput: {
    fontSize: 17,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pannierIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logOutText: {
    color: '#FFF',
    fontSize: 50,
  },
  textLogout: {
    color: '#FFF',
    fontSize: 20,
    textAlign: 'center',
  },
  itemInvisible: {
    backgroundColor: 'transparent',
  },
  itemText: {
    color: '#1e90ff',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  stretch: {
    width: 40,
    height: 50,
    borderRadius: 10,
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dataa: {
    margin: 10,
    width: 400,
  },
  searchStyle: {
    width: 350,
    height: 60,
    borderRadius: 10,
  },
});
