import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

export default function MapsScreen({navigation}) {
  const state = {
    latlng: {latitude: 33.8869, longitude: 9.5375},
  };
  const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      //height: 400,
      //width: 400,
      justifyContent: 'flex-end',

      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
  });
  return (
    <View style={styles.container}>
      <MapView
        mapType="standard"
        zoomEnabled={true}
        pitchEnabled={true}
        showsUserLocation={true}
        followsUserLocation={true}
        showsCompass={true}
        showsBuildings={true}
        showsTraffic={true}
        showsIndoors={true}
        style={styles.map}
        region={{
          latitude: 33.8869,
          longitude: 9.5375,
          latitudeDelta: 9,
          longitudeDelta: 9,
        }}>
        <Marker
          draggable
          coordinate={state.latlng}
          title="Home"
          onDragEnd={e => {
            console.log('dragEnd', e.nativeEvent.coordinate);
          }}
        />
      </MapView>
    </View>
  );
}
