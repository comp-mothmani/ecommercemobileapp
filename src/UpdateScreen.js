import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  ScrollView,
  Button,
  Alert,
  ActivityIndicator,
} from 'react-native';

export default class UpdateScreen extends Component {
  state = {
    currentPassword: '',
    newPassword: '',
    newEmail: '',
    newName: '',
    newFirstName: '',
    newNumber: '',
    newAddress: '',
    username: '',
    bol: '',
    name: '',
    firstname: '',
    adresse: '',
    phonenumber: '',
    id: '',
  };

  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <ScrollView
          style={{
            flex: 1,
            flexDirection: 'column',
            paddingVertical: 2,
            paddingHorizontal: 10,
          }}>
          <TextInput
            style={styles.textInput}
            value={this.state.currentPassword}
            placeholder="Current Password"
            autoCapitalize="none"
            secureTextEntry={true}
            onChangeText={text => {
              this.setState({currentPassword: text});
            }}
          />

          <TextInput
            style={styles.textInput}
            value={this.state.newPassword}
            placeholder="New Password"
            autoCapitalize="none"
            secureTextEntry={true}
            onChangeText={text => {
              this.setState({newPassword: text});
            }}
          />

          <Button
            title="Change Password"
            onPress={this.onChangePasswordPress}
          />

          <TextInput
            style={styles.textInput}
            value={this.state.newEmail}
            placeholder="New Email"
            autoCapitalize="none"
            keyboardType="email-address"
            onChangeText={text => {
              this.setState({newEmail: text});
            }}
          />

          <Button title="Change Email" onPress={this.onChangeEmailPress} />

          <Text style={{color: '#FFF'}}>
            Phone Number :{this.state.phonenumber}
          </Text>
          <TextInput
            style={styles.textInput}
            value={this.state.newNumber}
            placeholder="New Phone Number"
            autoCapitalize="none"
            keyboardType="phone-pad"
            onChangeText={text => {
              this.setState({newNumber: text});
            }}
          />

          <Button
            title="Change Phone Number "
            onPress={this.onChangephoneNumberPress}
          />

          <Text style={{color: '#FFF'}}> Name :{this.state.username}</Text>
          <TextInput
            style={styles.textInput}
            value={this.state.newName}
            placeholder="New Name"
            autoCapitalize="none"
            keyboardType="default"
            onChangeText={text => {
              this.setState({newName: text});
            }}
          />

          <Button title="Change Name" onPress={this.onChangeNamePress} />

          <Text style={{color: '#FFF'}}>
            Firt Name : {this.state.firstnrame}
          </Text>
          <TextInput
            style={styles.textInput}
            value={this.state.newFirstName}
            placeholder="New First Name"
            autoCapitalize="none"
            keyboardType="default"
            onChangeText={text => {
              this.setState({newFirstName: text});
            }}
          />

          <Button
            title="Change First Name"
            onPress={this.onChangeFirstNamePress}
          />

          <Text style={{color: '#FFF'}}>Address : {this.state.username}</Text>
          <TextInput
            style={styles.textInput}
            value={this.state.newAddress}
            placeholder="New Address"
            autoCapitalize="none"
            keyboardType="default"
            onChangeText={text => {
              this.setState({newAddress: text});
            }}
          />
          <Button title="Change Address" onPress={this.onChangeAddressPress} />
        </ScrollView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({});
