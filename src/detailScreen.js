import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  ImageBackground,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class detailScreen extends React.Component {
  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    headerShown: false,
  };

  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{width: '100%', height: '100%'}}>
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.backIcon}
            onPress={() => this.props.navigation.navigate('Home')}>
            <Icon name={'navigate-before'} color={'#FFF'} size={40} />
          </TouchableOpacity>

          <View style={styles.imageView} />

          <View style={styles.textView} />

          <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
            <Icon
              style={styles.achetezIcon}
              name={'add-shopping-cart'}
              color={'#FFF'}
              size={30}
            />
            <Text
              style={{
                color: '#FFF',
                fontWeight: '500',
                fontSize: 25,
                top: -15,
              }}>
              {' '}
              Achetez{' '}
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: -10,
  },
  button: {
    marginTop: 20,

    backgroundColor: '#1e90ff',
    borderRadius: 4,
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
    width: 390,
    marginLeft: 11,
  },
  backIcon: {
    top: 30,
    left: 20,
  },

  achetezIcon: {
    marginLeft: -230,
    top: 16,
  },
  imageView: {
    backgroundColor: '#FFF',
    width: 390,
    height: 300,
    marginTop: 40,
    marginLeft: 11,
  },

  textView: {
    backgroundColor: '#FFF',
    width: 390,
    height: 180,
    marginTop: 15,
    marginLeft: 11,
  },
});
