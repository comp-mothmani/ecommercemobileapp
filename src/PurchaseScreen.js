/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  Button,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class PurchaseScreen extends React.Component {
  constructor(props) {
    super(props);

    //setting default state
    this.state = {
      isLoading: true,
      text: '',
      subCategoryName: this.props.navigation.state.params.subCategoryName,
      subCategoryPic: this.props.navigation.state.params.subCategoryPic,
      subCategoryDescription: this.props.navigation.state.params
        .subCategoryDescription,
      subCategoryPrice: this.props.navigation.state.params.subCategoryPrice,
      subCategoryId: this.props.navigation.state.params.subCategoryId,
    };
    this.arrayholder = [];
  }

  addToCartFunc() {
    const obj = {
      productName: this.state.subCategoryName,
      productPrice: this.state.subCategoryPrice,
      productPic: this.state.subCategoryPic,
      productQuantity: 1,
      productId: this.state.subCategoryId,
    };
    const defaultObj = {
      defaultProductName: this.state.subCategoryName,
      defaultProductPrice: this.state.subCategoryPrice,
    };

    // check if element exist in list
    var exist = false;
    for (var i = 0; i < global.listOfProducts.length; i++) {
      var name = global.listOfProducts[i].productName;
      if (name == obj.productName) {
        exist = true;
        break;
      } else {
        exist = false;
      }
    }
    if (!exist) {
      global.listOfProducts.push(obj);
      global.defaultListOfProducts.push(defaultObj);
    }

    this.props.navigation.navigate('Basket');
  }

  componentDidMount() {}

  static navigationOptions = {
    headerShown: false,
  };
  state = {
    value: '',
  };

  render() {
    return (
      <ImageBackground
        source={require('../picture/3.jpg')}
        style={{
          width: '100%',
          height: '100%',
        }}>
        <View style={styles.container1}>
          <Image
            source={{uri: 'data:image/png;base64,' + this.state.subCategoryPic}}
            style={styles.productImageStyle}
          />
        </View>

        <View style={styles.container2}>
          <Text style={styles.productNameStyle}>
            {this.state.subCategoryName}
          </Text>
          <Text style={styles.productDescriptionStyle}>
            {this.state.subCategoryDescription}
          </Text>
          <Text style={styles.productPriceStyle}>
            Price: {this.state.subCategoryPrice}
          </Text>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.addToCartFunc()}>
          <Text style={styles.text}> Add to Cart </Text>

          <Icon
            style={styles.addicon}
            name={'add-shopping-cart'}
            color={'#FFF'}
            size={25}
          />
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container1: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container2: {
    flex: 0.8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container3: {
    flex: 0.1,
    flexDirection: 'row',
  },
  productImageStyle: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  productNameStyle: {
    color: 'white',
    margin: 20,
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  productDescriptionStyle: {
    color: 'white',
    margin: 20,
    textAlign: 'center',
    fontSize: 15,
  },
  productPriceStyle: {
    color: 'orange',
    margin: 20,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },
  productPurchaseButton: {
    width: '100%',
  },
  button: {
    backgroundColor: '#1e90ff',
    height: 50,
  },
  text: {
    fontSize: 24,
    textAlign: 'center',
    color: '#FFF',
    top: 5,
  },
  addicon: {
    top: -22,
    marginLeft: 90,
  },
});
